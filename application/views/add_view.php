<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('components/head.php'); ?>
	<title>Add data</title>
</head>

<body class="flex flex-col items-center justify-center w-screen h-screen gap-10 bg-blue-100">
	<h1 class="text-3xl">Add data</h1>
	<div class="w-1/2">
		<form class="flex flex-col items-start justify-center w-full h-auto gap-4 p-10 border-2 border-slate-400 rounded-xl" method="post">
			<div class="flex flex-col w-full">
				<label for="nama">Nama</label>
				<input class="px-2 py-2 rounded-lg" type="text" name="nama" placeholder="masukkan nama" />
			</div>

			<div class="flex flex-col w-full">
				<label for="alamat">Alamat</label>
				<input class="px-2 py-2 rounded-lg" type="text" name="alamat" placeholder="masukkan alamat" />
			</div>

			<div class="flex flex-col w-full">
				<label for="nomor">Nomor Telepon</label>
				<input class="px-2 py-2 rounded-lg" type="text" name="nomor" placeholder="masukkan nomor" />
			</div>
			<div class="flex flex-row gap-2">
				<button type="submit" class='px-4 py-2 font-normal normal-case bg-green-500 rounded-lg'>Submit</button>
				<a href="<?= base_url('page/') ?>" class="px-4 py-2 text-lg font-normal normal-case bg-red-500 rounded-lg">Cancel</a>
			</div>
		</form>

	</div>

</body>

</html>