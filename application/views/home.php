<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('components/head.php'); ?>
	<title>Home</title>
</head>

<body class="flex flex-col items-center justify-start w-full h-screen gap-5 mt-20 bg-blue-100">
	<h1 class="text-3xl font-semibold">Buku telepon</h1>

	<div>
		<a href="<?= base_url('page/add') ?>" class='px-4 py-2 text-black bg-blue-300 rounded-lg'>Add data</a>
	</div>



	<table class="border border-collapse table-auto border-spacing-2 border-slate-400">
		<thead>
			<tr>

				<th>Nama</th>
				<th>Alamat</th>
				<th>Nomor Telepon</th>
				<th>Actions</th>
			</tr>
		</thead>

		<?php

		foreach ($data as $row) : ?>
			<tr class="h-10">

				<td><?= $row->nama ?></td>
				<td><?= $row->alamat ?></td>
				<td><?= $row->no_telepon ?></td>
				<td> <a class='px-4 py-2 text-black bg-green-300 rounded-lg' href="<?= base_url('page/update/'. $row->id)  ?>">Update</a></td>
				<td> <a class='px-4 py-2 text-black bg-red-300 rounded-lg' href="<?= base_url('page/delete/'. $row->id) ?>">Delete</a></td>
			</tr>


		<?php endforeach ?>

	</table>

</body>

</html>