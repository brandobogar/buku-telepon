<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('components/head.php'); ?>
	<title>Edit Data</title>
</head>

<body class="flex flex-col items-center justify-center w-screen h-screen bg-blue-100">
	<div class="flex flex-col items-center w-1/2">
		<h1 class="mb-4 text-3xl">Edit Data: <?= $data->nama ?> </h1>
		<form class="flex flex-col w-full gap-4 p-10 border-2 border-slate-400 rounded-xl" method="post">
			<div class="flex flex-col">
				<label for="nama">Nama</label>
				<input class="px-2 py-2 rounded-lg" type="text" id="nama" name="nama" value="<?= $data->nama ?>" />
			</div>
			<div class="flex flex-col">
				<label for="alamat">Alamat</label>
				<input class="px-2 py-2 rounded-lg" type="text" id="alamat" name="alamat" value="<?= $data->alamat ?>" />
			</div>
			<div class="flex flex-col">
				<label for="nomor">Nomor</label>
				<input class="px-2 py-2 rounded-lg" type="text" id="nomor" name="no_telepon" value="<?= $data->no_telepon ?>" />
			</div>
			<div class="flex flex-row gap-2">
				<button type="submit" class="px-4 py-2 text-lg font-normal normal-case bg-green-500 rounded-lg">Update</button>
				<a href="<?= base_url('page/') ?>" class="px-4 py-2 text-lg font-normal normal-case bg-red-500 rounded-lg">Cancel</a>
			</div>
		</form>
	</div>
</body>

</html>