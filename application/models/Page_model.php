<?php

class Page_model extends CI_Model
{
  private $_table = 'buku_telepon';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }
  public function rules()
  {
    return [
      [
        'field' => 'nama',
        'label' => 'Nama',
        'rules' => 'required|max_length[32]'
      ],
      [
        'field' => 'alamat',
        'label' => 'alamat',
        'rules' => 'required|valid_email|max_length[32]'
      ],
      [
        'field' => 'nomor',
        'label' => 'nomor',
        'rules' => 'required|valid_email|max_length[32]'
      ],
    ];
  }



  public function display_records()
  {
    $query = $this->db->get("buku_telepon");
    return $query->result();
  }

  public function delete($id)
  {
    if (!$id) {
      return;
    }

    return $this->db->delete('buku_telepon', ['id' => $id]);
  }

  public function insert($buku_telepon)
  {
    return $this->db->insert('buku_telepon', $buku_telepon);
  }


  public function update($data)
  {
    $this->db->where('id', $data['id']);
    return $this->db->update('buku_telepon', $data);
  }

  public function find($id)
  {
    $query = $this->db->get_where('buku_telepon', ['id' => $id]);
    return $query->row();
  }
}
