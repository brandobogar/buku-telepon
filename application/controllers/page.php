<?php


class Page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Page_model');
	}

	public function index()
	{
		$result['data'] = $this->Page_model->display_records();
		$this->load->view('home', $result);
	}

	public function update($id)
	{
		$this->load->model('Page_model');
		$result['data'] = $this->Page_model->find($id);

		if ($this->input->method() === "post") {
			$data['id'] = $id;
			$data['nama'] = $this->input->post('nama');
			$data['alamat'] = $this->input->post('alamat');
			$data['no_telepon'] = $this->input->post('no_telepon');

			$this->Page_model->update($data);

			redirect('/');
		}

		$this->load->view('edit_view', $result);
	}

	public function add()
	{

		if ($this->input->method() === "post") {
			$this->load->model('Page_model');
			$buku_telepon = [

				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'no_telepon' => $this->input->post('nomor'),
			];

			$saved = $this->Page_model->insert($buku_telepon);
			redirect('/');
		}

		$this->load->view('add_view');
	}

	public function delete($id)
	{
		$this->Page_model->delete($id);
		redirect('/');
	}
}
